import settings
import urllib
import json
import time

from celery import Celery
from celery.utils import LOG_LEVELS

from pymongo import Connection

from signals import event_log
from pysignals import receiver

celery = Celery(
     'tasks',
    broker=settings.BROKER_URL
)

celery.conf.CELERY_TASK_SERIALIZER = 'json'
celery.conf.CELERYD_LOG_LEVEL = LOG_LEVELS['INFO']

cn = Connection()
db = cn.likes

db.items.drop()
db.progress.drop()


@receiver(event_log)
def notify_finish_post(sender, time, post_id, **kwargs):
    fp = open('events.log', 'ar')
    message = '%s: %s\n' % (post_id, time)
    fp.write(message)


@celery.task
def get_likes(post_id, url=None, start_time=None, total_likes=0):

    if not start_time:
        start_time = time.time()
    else:
        start_time = float(start_time)

    if not url:
        params = dict(
            limit=settings.LIKES_PER_PAGE,
            access_token=settings.ACCESS_TOKEN,
        )

        url = '%s/%s/likes?%s' % (
            settings.FACEBOOK_GRAPH,
            post_id,
            urllib.urlencode(params)
        )

    data = json.loads(urllib.urlopen(url).read())
    likes = data.get('data')

    if likes:

        for like in likes:

            user_id = like.get('id')
            item = db.items.find_one({'user_id': user_id})
            count = 1

            if not item:
                db.items.insert(dict(
                    user_id=user_id,
                    count=count
                ))
            else:
                db.items.update(
                    {'user_id': user_id},
                    {'$inc': {'count': count}}
                )

            post = db.progress.find_one({'post_id': post_id})
            count = int(post.get('count')) + 1 if post else 1

            if count > total_likes:
                return

            percent = (count * 100) / total_likes

            if not post:
                db.progress.insert(dict(
                    post_id=post_id,
                    percent=percent,
                    count=count)
                )
            else:
                db.progress.update(
                    {'post_id': post_id}, {
                        '$inc': {'count': 1},
                        '$set': {'percent': percent}
                    }
                )

        paging = data.get('paging')

        if 'next' in paging:
            get_likes.delay(
                post_id=post_id,
                url=paging.get('next'),
                start_time=str(start_time),
                total_likes=total_likes
            )

    else:
        db.progress.update(
            {'post_id': post_id}, {
                '$set': {'percent': 100}
            }
        )

    event_log.send(
        sender=None,
        post_id=post_id,
        time=str(time.time() - start_time)
    )
