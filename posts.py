import settings
import json
import urllib
import time
import logging

from datetime import datetime
from tasks import get_likes

from pymongo import Connection

cn = Connection()
db = cn.likes

db.metadata.drop()
db.metadata.insert(dict(id=1))

total_posts = 0
total_posts_likes = 0


def posts(url=None):

    global total_posts
    global total_posts_likes

    if not url:
        params = dict(
            limit=settings.POSTS_PER_PAGE,
            access_token=settings.ACCESS_TOKEN,
            fields='likes'
        )

        url = '%s/%s/posts?%s' % (
            settings.FACEBOOK_GRAPH,
            settings.ID_PAGE,
            urllib.urlencode(params)
        )

    try:
        data = json.loads(urllib.urlopen(url).read())
    except IOError as exc:
        logging.info(exc)
        time.sleep(settings.SECONDS_RETRY_POST)
        posts(url)

    items = data.get('data')

    if items:

        for x in items:
            db.metadata.update(
                {'id': 1}, {
                    '$inc': {'count_posts': 1}
                }
            )

            if 'likes' in x.keys():

                db.metadata.update(
                    {'id': 1}, {
                        '$inc': {'count_likes': 1}
                    }
                )

                get_likes.delay(
                    post_id=x.get('id'),
                    total_likes=x.get('likes').get('count')
                )

        paging = data.get('paging')

        if 'next' in paging:
            posts(url=paging.get('next'))


if __name__ == "__main__":
    tstart = datetime.now()
    posts()
    tend = datetime.now()
    print 'estimated_time: %s' % (tend - tstart)
