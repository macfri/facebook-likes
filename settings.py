ID_PAGE = ''

POSTS_PER_PAGE = 5
LIKES_PER_PAGE = 50

STOP_NEXT_PAGE = False

FACEBOOK_GRAPH = 'https://graph.facebook.com'
ACCESS_TOKEN = ''
BROKER_URL = "mongodb://localhost:27017/<Your Database>"

SECONDS_RETRY_POST = 5

try:
    from local_settings import *
except ImportError:
    pass
