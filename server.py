from pymongo import Connection
from flask import Flask, jsonify, render_template

cn = Connection()
db = cn.likes
app = Flask(__name__)
app.debug = True


@app.route('/get_metadata')
def get_metadata():
    item = db.metadata.find_one({'id': 1})

    count_likes = '0'
    count_posts = '0'

    if item:

        if 'count_posts' in item:
            count_posts = item['count_posts']


        if 'count_likes' in item:
            count_likes = item['count_likes']

    return jsonify(
        count_posts=count_posts,
        count_likes=count_likes,
    )


@app.route('/get_percent/<post_id>')
def get_percent(post_id):
    item = db.progress.find_one(
        {'post_id': post_id},
        {'percent'}
    )
    value = str(item['percent']) if item else '0'
    return jsonify(value=value)


@app.route('/progress_bar')
def progress_bar():

    #db.progress.find({'percent': {'$lt': 100}})

    data = []
    for x, y in enumerate(db.progress.find({'percent': {'$lt': 100}})):
        data.append(dict(
            n=x,
            post=y.get('post_id'),
            percent=y.get('percent')
        ))
    return jsonify(data=data)


@app.route('/')
def index():
    return render_template(
        'index.html',
        items=enumerate(db.progress.find())
    )


if __name__ == '__main__':
    app.run()
